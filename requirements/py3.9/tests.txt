#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.9/tests.txt requirements/tests.in
#
acct==8.6.0
    # via
    #   idem
    #   pop-evbus
aiofiles==23.1.0
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
asynctest==0.13.0
    # via -r requirements/tests.in
cffi==1.15.1
    # via cryptography
colorama==0.4.6
    # via
    #   idem
    #   rend
cryptography==41.0.1
    # via acct
dict-toolbox==3.1.2
    # via
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pop-tree
    #   pytest-pop
    #   rend
docstring-parser==0.15
    # via pop-tree
exceptiongroup==1.1.2
    # via pytest
idem==23.0.3
    # via
    #   -r requirements/base.txt
    #   -r requirements/tests.in
iniconfig==2.0.0
    # via pytest
jinja2==3.1.2
    # via
    #   idem
    #   rend
jmespath==1.0.1
    # via idem
lazy-object-proxy==1.9.0
    # via pop
markupsafe==2.1.3
    # via jinja2
mock==5.0.2
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.5
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
nest-asyncio==1.5.6
    # via
    #   pop-loop
    #   pytest-pop
packaging==23.1
    # via pytest
pluggy==1.2.0
    # via pytest
pop-config==12.0.2
    # via
    #   acct
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==6.2.3
    # via idem
pop-loop==1.1.0
    # via
    #   idem
    #   pop
pop-serial==1.1.1
    # via
    #   acct
    #   idem
    #   pop-evbus
pop-tree==10.2.1
    # via idem
pop==24.0.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
pycparser==2.21
    # via cffi
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.18.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-pop==12.0.0
    # via -r requirements/tests.in
pytest==7.4.0
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0.1
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   rend
rend==6.5.2
    # via
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
sniffio==1.3.0
    # via pop-loop
toml==0.10.2
    # via
    #   idem
    #   rend
tomli==2.0.1
    # via pytest
tqdm==4.65.0
    # via idem
uvloop==0.17.0
    # via idem
wheel==0.40.0
    # via idem
