==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

===========
idem-random
===========

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/


An Idem plugin that provides randomness.

About
=====
Idem Random provides us with logical states like random password,
random integer, random string which can be used in conjunction with
other idem plugins. This plugin in itself doesn't interact with the
any cloud provider.

An example use-case where this plugin can be used is -
AWS RDS database instance needs a password when its created.
We can use AWS's RDS state along with Idem random password state
in order to generate a random password at runtime and help create
the RDS instance.


Random ID
------------
The resource random_id generates random numbers that are intended to be used as unique identifiers for other resources.
This resource does use a cryptographic random number generator in order to minimize the chance of collisions, making the
results of this resource when a 16-byte identifier is requested of equivalent uniqueness to a type-4 UUID.

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/saltstack/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/saltstack/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.8+
* git *(if installing from source, or contributing to the project)*
* Idem

.. note::
  It is recommended that you install Idem using Poetry. Poetry is a tool for virtual environment and dependency management. See the `Idem Getting Started guide <https://docs.idemproject.io/getting-started/en/latest/topics/gettingstarted/installing.html>`_ for more information.

Installation
------------

You can install ``idem-random`` from PyPI, a source repository, or a local directory.

Before you install ``idem-random``, ensure that you are in the same directory as your ``pyproject.toml`` file. Optionally, you can specify the directory containing your ``pyproject.toml`` file by using the ``--directory=DIRECTORY (-C)`` option.

Install from PyPI
+++++++++++++++++

To install ``idem-random`` from PyPI, run the following command:

.. code-block:: bash

  poetry add idem-random

Install from source
+++++++++++++++++++

You can also install ``idem-random`` directly from the source repository:

.. code-block:: bash

  poetry add git+https://gitlab.com/vmware/idem/idem-random.git

If you don't specify a branch, Poetry uses the latest commit on the ``master`` branch.

Install from a local directory
++++++++++++++++++++++++++++++

Clone the ``idem-random`` repository. Then run the following command to install from the cloned directory:

.. code-block:: bash

  poetry add ~/path/to/idem-random

Roadmap
=======

Reference the `open issues <https://gitlab.com/vmware/idem/idem-random/issues>`__ for a list of
proposed features (and known issues).

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.
