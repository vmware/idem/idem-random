import pytest

from tests.integration.states import run_sls

esm_cache = {}

STATE_PWD_PRESENT = """
random_password:
  random.password.present:
    - name: rp
    - length: 5
"""

STATE_PWD_PRESENT_WITH_RESOURCE_ID = """
random_password_with_resource_id:
  random.password.present:
    - name: rp
    - length: 5
    - resource_id: rj2j3
"""


STATE_PWD_ABSENT = """
random_password:
  random.password.absent:
    - name: rp
"""


def test_password_present(hub):
    running = run_sls(STATE_PWD_PRESENT, managed_state=esm_cache)
    ret = running["random.password_|-random_password_|-random_password_|-present"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    assert len(new_state["output"]) == 5

    running = run_sls(STATE_PWD_PRESENT_WITH_RESOURCE_ID, managed_state=esm_cache)
    ret = running[
        "random.password_|-random_password_with_resource_id_|-random_password_with_resource_id_|-present"
    ]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    assert len(new_state["output"]) == 5
    assert new_state["resource_id"] == "rj2j3"


@pytest.mark.depends(on=["test_password_present"])
def test_password_absent(hub):
    running = run_sls(STATE_PWD_ABSENT, managed_state=esm_cache)
    ret = running["random.password_|-random_password_|-random_password_|-absent"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"] is None
    assert len(ret["old_state"]["output"]) == 5
