def test_generate_random_string(mock_hub, hub):

    mock_hub.exec.random.password.generate_random_string = (
        hub.exec.random.password.generate_random_string
    )

    ret = mock_hub.exec.random.password.generate_random_string(5)

    assert ret
    assert ret["ret"]
    assert ret["result"]
