.. idem-random documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem-random's Documentation!
=======================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/idem-random
   tutorial/index
   releases/index

.. include:: /_includes/reference-toc.rst

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/idem/idem-random/>

Indices and tables
==================

* :ref:`modindex`
